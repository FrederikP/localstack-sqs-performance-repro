# pip install boto3

import json
import os
import random
import time

import boto3
import botocore

import concurrent.futures

concurrent_connections = 50

boto_config = botocore.config.Config(max_pool_connections=concurrent_connections)

endpoint = os.environ.get('AWS_ENDPOINT', 'http://localhost:9324')

print(f"Using SQS endpoint: {endpoint}")

sqs = boto3.client('sqs',
                    endpoint_url=endpoint,
                    region_name='benchmarkregion',
                    aws_secret_access_key='x',
                    aws_access_key_id='x',
                    use_ssl=False,
                    config=boto_config)

queue_name = 'testqueue'

queue_creation_response = sqs.create_queue(
    QueueName=queue_name
)

queue_url = queue_creation_response['QueueUrl']

send_durations = []

def send():
    t0 = time.time()
    sqs.send_message(
        QueueUrl=queue_url,
        MessageBody=json.dumps({'testkey': 'testvalue' * random.randint(1, 100)})
    )
    t1 = time.time()
    send_durations.append(t1-t0)

number_of_messages = 1000

print(f"number of events to send: {number_of_messages}")

print(f"sending in parallel with {concurrent_connections} concurrent connections/workers")
t0 = time.time()
with concurrent.futures.ThreadPoolExecutor(max_workers=concurrent_connections) as executor:
    futures = []
    for i in range(number_of_messages):
        futures.append(executor.submit(send))
    for future in futures:
        future.result()
t1 = time.time()
overall_send_duration = t1 - t0
average_send_duration = sum(send_durations) / len(send_durations)
print(f"sending finished, avg duration: {average_send_duration:.3f}s, total duration: {overall_send_duration:.3f}")

print("receiving+deleting sequentially - one by one")
receive_durations = []
delete_durations = []
stop = False
counter = 0
overall_t0 = time.time()
while not stop:
    t0_receive = time.time()
    msgs = sqs.receive_message(
        QueueUrl=queue_url,
    )
    t1_receive = time.time()
    if 'Messages' not in msgs or not msgs['Messages']:
        stop = True
    else:
        receive_durations.append(t1_receive-t0_receive)
        counter += len(msgs['Messages'])
        for msg in msgs['Messages']:
            t0_delete = time.time()
            sqs.delete_message(
                QueueUrl=queue_url,
                ReceiptHandle=msg['ReceiptHandle']
            )
            t1_delete = time.time()
            delete_durations.append(t1_delete-t0_delete)

overall_t1 = time.time()
overall_receive_duration = overall_t1 - overall_t0
average_duration_receive = sum(receive_durations) / len(receive_durations)
average_duration_delete = sum(delete_durations) / len(delete_durations)
print(f"received: {counter} of {number_of_messages}")
print(f"receiving+deleting finished, avg duration receive: {average_duration_receive:.3f}s, avg duration delete: {average_duration_delete:.3f}s, total duration: {overall_receive_duration:.3f}")
assert counter == number_of_messages
