# Link to github issue:

https://github.com/localstack/localstack/issues/3998

# Run locally

Starting dependencies requires docker

Run in individual terminals:

```
docker run --rm -p 9324:9324 -p 9325:9325 softwaremill/elasticmq-native
```

```
docker run --rm -it -p 4566:4566 -p 4571:4571 -e 'SERVICES=sqs' localstack/localstack
```

Script requires python3

```
pip install boto3
AWS_ENDPOINT=http://localhost:4566/ python benchmark.py
AWS_ENDPOINT=http://localhost:9324/ python benchmark.py
```

# CI

config in `.gitlab-ci.yml`